FROM node:14.21.3-bullseye

# install npm packaged
USER 0:0
RUN npm install forever -g
RUN npm install jest

# create user and home dir
RUN groupadd -r -g 999 docker
RUN useradd -m -d /home/node/app -u 999 -g docker -s /bin/bash -r docker
RUN mkdir -p /home/node/app/
#RUN chown -R 999.999 /home/node
#RUN chown -R 999:999 /home/node

# change user and dir
#USER 999:999
WORKDIR /home/node/app

# add files 
ADD . .
ADD tests/* ./
RUN chown -R 999:999 /home/node

# run stuff
USER 999:999
RUN npm install express && npm install websocket && npm install
RUN cd ./public && npm install && npm run sass && npm run coffee

# entrypoint
ENTRYPOINT ["./start.sh"]
