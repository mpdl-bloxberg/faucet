module.exports = function (app) {
  var EthereumTx = app.EthereumTx;
  var generateErrorResponse = app.generateErrorResponse;
  var config = app.config;
  var configureWeb3 = app.configureWeb3;
  var validateCaptcha = app.validateCaptcha;
  const rateLimit = app.rateLimit;
  const requestIp = app.requestIp;

  //limiter for limiting the number of request based on ip
  const limiter = rateLimit({
    windowMs: 1440 * 60 * 1000, // 24 hours
    max: 2, // limit each IP to 2 requests per windowMs
    message: "You have made too many requests for bergs",
    keyGenerator: (request, response) => requestIp.getClientIp(request),
    skip: (request, response) => {
      console.log("captcha response: " + !request.body["g-recaptcha-response"]);
      return !request.body["g-recaptcha-response"];
    },
  });

  /**
   * function call for captcha authentication and retrieving the bergs
   * @param  {} limiter
   */
  app.post("/getBergs", limiter, async function (request, response) {
    console.log("request", request);
    console.log("requestIp", requestIp.getClientIp(request));
    if ("rateLimit" in request && request.rateLimit.remaining < 0)
      return generateErrorResponse(response, {
        code: 429,
        title: "Error",
        message: "Too many requests",
      });

    console.log("REQUEST:");
    console.log(request.body);
    var recaptureResponse = request.body["g-recaptcha-response"];
    if (!recaptureResponse)
      return generateErrorResponse(response, {
        code: 500,
        title: "Error",
        message: "Invalid captcha",
      });

    var receiver = request.body.receiver;
    let out;
    try {
      out = await validateCaptcha(recaptureResponse);
    } catch (e) {
      return generateErrorResponse(response, e);
    }
    await validateCaptchaResponse(out, receiver, response);
  });

  /**
   * function call to check the health
   * @returns balanceInWei , balanceInEth and address
   */
  app.get("/health", async function (request, response) {
    let web3;
    try {
      web3 = await configureWeb3(config);
    } catch (e) {
      return generateErrorResponse(response, e);
    }
    let resp = {};
    resp.address = config.Ethereum[config.environment].account;
    let balanceInWei = await web3.eth.getBalance(resp.address);
    let balanceInEth = await web3.utils.fromWei(balanceInWei, "ether");
    resp.balanceInWei = balanceInWei;
    resp.balanceInEth = Math.round(balanceInEth);
    response.send(resp);
  });

  /**
   * To validate Captcha Response
   */
  async function validateCaptchaResponse(out, receiver, response) {
    if (!out)
      return generateErrorResponse(response, {
        code: 500,
        title: "Error",
        message: "Invalid captcha",
      });
    if (!out.success)
      return generateErrorResponse(response, {
        code: 500,
        title: "Error",
        message: "Invalid captcha",
      });

    let web3;
    try {
      web3 = await configureWeb3(config);
    } catch (e) {
      return generateErrorResponse(response, e);
    }
    await sendPOAToRecipient(web3, receiver, response);
  }

  /**
   * function to send bergs to the clinet address
   * @param  {} web3 send the web3 configuration
   * @param  {} receiver adress for the receiver
   * @param  {} response
   */
  async function sendPOAToRecipient(web3, receiver, response) {
    let senderPrivateKey = config.Ethereum[config.environment].privateKey;
    const privateKeyHex = Buffer.from(senderPrivateKey, "hex");
    if (!web3.utils.isAddress(receiver))
      return generateErrorResponse(response, {
        code: 500,
        title: "Error",
        message: "invalid address",
      });

    const gasPrice = web3.utils.toWei("1", "gwei");
    let gasPriceHex = web3.utils.toHex(gasPrice);
    let gasLimitHex = web3.utils.toHex(config.Ethereum.gasLimit);
    let nonce = await web3.eth.getTransactionCount(
      config.Ethereum[config.environment].account
    );
    let nonceHex = web3.utils.toHex(nonce);
    let BN = web3.utils.BN;
    let ethToSend = web3.utils.toWei(
      new BN(config.Ethereum.milliEtherToTransfer),
      "milliether"
    );

    const rawTx = {
      nonce: nonceHex,
      gasPrice: gasPriceHex,
      gasLimit: gasLimitHex,
      to: receiver,
      value: ethToSend,
      data: "0x00",
    };

    let tx = new EthereumTx(rawTx);
    tx.sign(privateKeyHex);

    let serializedTx = tx.serialize();

    let txHash;
    web3.eth
      .sendSignedTransaction("0x" + serializedTx.toString("hex"))
      .on("transactionHash", (_txHash) => {
        txHash = _txHash;
      })
      .on("receipt", (receipt) => {
        console.log(receipt);
        if (receipt.status == "0x1") {
          return sendRawTransactionResponse(txHash, response);
        } else {
          let err = {
            code: 500,
            message: "Transaction is mined, but status is false",
          };
          return generateErrorResponse(response, err);
        }
      })
      .on("error", (err) => {
        return generateErrorResponse(response, err);
      });
  }

  function sendRawTransactionResponse(txHash, response) {
    var successResponse = {
      code: 200,
      title: "Success",
      message: "Tx is mined",
      txHash: txHash,
    };

    response.send({ success: successResponse });
  }
};
