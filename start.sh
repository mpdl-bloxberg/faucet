#!/bin/bash

source /env/faucet.env

cd /home/node/app

[[ -z "$CAPTCHA_KEY" ]] && {
	export CAPTCHA_KEY="6LehE3UUAAAAAMG1lwAiLBm5379wtSsuYgVd_0yW" 
	export CAPTCHA_SITE_KEY="6LehE3UUAAAAAOBbg-bj0YKNKNZv9-MALY8-YMin"
}

sed -i s/"__CAPTCHA_SITE_KEY__"/"${CAPTCHA_SITE_KEY}"/ ./public/index.html

sed -i s/"__CAPTCHA_KEY__"/"${CAPTCHA_KEY}"/ config.json
sed -i s/"__RPC_PROTO__"/"${RPC_PROTO}"/ config.json
sed -i s/"__RPC_HOST__"/"${RPC_HOST}"/ config.json
sed -i s/"__RPC_PORT__"/"${RPC_PORT}"/ config.json
sed -i s/"__ACCOUNT__"/${ACCOUNT}/ config.json
sed -i s/"__PRIVATE_KEY__"/${PRIVATE_KEY}/ config.json

/usr/bin/env forever index.js

