#!/bin/python
from pyvirtualdisplay import Display
from selenium import webdriver
import time
from selenium.webdriver.common.by import By
from selenium.common.exceptions import NoSuchElementException

def request_bergs(driver, captcha=True):
    # enter wallet address
    address = driver.find_element(By.ID, "receiver")
    address.clear()
    address.send_keys("0x99B369312b5975EDF98d4322612229b035C99bAE")

    if captcha:
        # switch to captcha and back
        driver.switch_to.frame(driver.find_element(By.TAG_NAME, "iframe"))
        driver.find_element(By.XPATH, "//span[@id='recaptcha-anchor']").click()
        driver.switch_to.default_content()

        # wait for captcha to complete
        time.sleep(5)

    # click to request
    button = driver.find_element(By.ID, "requestTokens")
    button.click()

    # wait for request to be handled
    time.sleep(20)

    # check if request gave tokens
    status = driver.find_element(By.ID, "swal2-title")
    info = driver.find_element(By.ID, "swal2-content")
    
    success = True
    if status.text == "Error":
        success = False;
        
    return (success, info.text)


print("end to end tests:")

# display = Display(visible=False, size=(1920,1080))
# display.start()

# dirver option headless
opt = webdriver.FirefoxOptions()
opt.add_argument("--headless")

url = "http://faucet-auto-deploy:3000"
# url = "https://faucet.qa.nut.test.bloxberg.org/"

# create webdriver
print("create driver")
try: 
    driver = webdriver.Firefox(options=opt)
    driver.get(url)
    print("SUCCESS: {}".format(url))
except Exception as e:
    print(e)
    print("FAILED: {}".format(url))
    exit(1)


# test 1
print("test [1/4]: requesting bergs without captcha")
ret, txt = request_bergs(driver, captcha=False) 
if ret == True:
    driver.close()
    print("FAILED: {}".format(txt))
    exit(1)
else: 
    print("SUCCESS: {}".format(txt))

ok = driver.find_element(By.XPATH, "//*/button[contains(text(),'OK')]").click()

# test 2
print("test [2/4]: requesting bergs first time")
ret, txt = request_bergs(driver) 
if ret == False:
    driver.close()
    print("FAILED: {}".format(txt))
    exit(1)
else: 
    print("SUCCESS: {}".format(txt))


ok = driver.find_element(By.XPATH, "//*/button[contains(text(),'OK')]").click()


# test 3
print("test [3/4]: requesting bergs second time")
ret, txt = request_bergs(driver) 
if ret == False:
    driver.close()
    print("FAILED: {}".format(txt))
    exit(1)
else: 
    print("SUCCESS: {}".format(txt))


driver.find_element(By.XPATH, "//*/button[contains(text(),'OK')]").click()


# test 4
print("test [4/4]: requesting bergs third time")
ret, txt = request_bergs(driver) 
if ret == True:
    driver.close()
    print("FAILED: {}".format(txt))
    exit(1)
else: 
    print("SUCCESS: {}".format(txt))

exit(0)
